import 'package:flutter/material.dart';

class PortfolioPage extends StatefulWidget {
  const PortfolioPage({super.key});

  @override
  State<PortfolioPage> createState() => _PortfolioPageState();
}

class _PortfolioPageState extends State<PortfolioPage> {
  int? count = 0;

  void increament() {
    setState(() {
      count = count! + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Portfolio",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 50,
            ),
            (count! >= 1)
                ? Container(
                    child: Column(
                      children: [
                        const Text(
                          "Name : Khushal Parmar",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w400),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        (count! >= 2)
                            ? Container(
                                child: Column(
                                  children: [
                                    Image.asset(
                                      "assets/images/profile.jpeg",
                                      height: 100,
                                    ),
                                  ],
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  )
                : Container(),
            const SizedBox(
              height: 50,
            ),
            Container(
              child: Column(
                children: [
                  (count! >= 3)
                      ? Container(
                          child: Column(
                            children: [
                              const Text(
                                "College : Sinhgad College Of Managment And Computer Application",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w400),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              (count! >= 4)
                                  ? Container(
                                      child: Column(
                                        children: [
                                          Image.network(
                                            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRs70qTfQXPkVvRu0fUGzj5b83QJdE70K9m-qZRMwDls9uppWyNCOYWQKMxPOTDYz2YZD4&usqp=CAU",
                                          ),
                                        ],
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            Container(
              child: Column(
                children: [
                  (count! >= 5)
                      ? Container(
                          child: Column(
                            children: [
                              const Text(
                                "Dream Company : Google",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w400),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              (count! >= 6)
                                  ? Container(
                                      child: Column(
                                        children: [
                                          Image.network(
                                            "https://www.freepnglogos.com/uploads/google-logo-png/google-logo-icon-png-transparent-background-osteopathy-16.png",
                                            height: 100,
                                          ),
                                        ],
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: increament,
        child: const Text("ADD"),
      ),
    );
  }
}
